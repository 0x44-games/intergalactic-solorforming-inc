use coffee::{Game, Result, Timer};
use coffee::graphics::{Batch, Color, Frame, Image, Point, Rectangle, Sprite, Window, WindowSettings, Vector};
use coffee::input::{KeyboardAndMouse, mouse};
use coffee::load::{loading_screen::ProgressBar, Join, Task};
use coffee::ui::{Column, Element, Justify, Renderer, Text, UserInterface};

use rand::prelude::*;
use rayon::prelude::*;

mod body;
use body::Body;
mod particles;
use particles::ParticleGroup;

fn main() -> Result<()> {
    <GameState as UserInterface>::run(WindowSettings {
        title: String::from("Galactic Planetary Defense Force"),
        size: (1280, 1024),
        resizable: false,
        fullscreen: false,
        maximized: false,
    })
}

struct GameState {
    bodies: Vec<Body>,
    particle_groups: Vec<ParticleGroup>,
    batch: Batch,
    score: u32,
    mouse_clicked: bool,
    mouse_start: Point,
    mouse_end: Point,
}

impl GameState {
    const SPRITE_WIDTH: u16 = 10;
    const SPRITE_HEIGHT: u16 = 10;

    fn generate(width: f32, height: f32) -> Task<Vec<Body>> {
        Task::succeed(move || {
            vec![
                // Sun
                Body::new(
                    20,
                    100,
                    Point::new(width / 2., height / 2.),
                    Vector::new(0., 0.),
                    [1, 1],
                    true
                ),
            ]
        })
    }

    fn load_graphics() -> Task<Image> {
        Task::using_gpu(move |gpu| Image::new(gpu, "./planets.png"))
    }
}

impl Game for GameState {
    type Input = KeyboardAndMouse;
    type LoadingScreen = ProgressBar;

    fn load(window: &Window) -> Task<GameState> {
        (
            Task::stage("Generating solar system...", Self::generate(window.width(),
                                                                     window.height())),
            Task::stage("Loading assets...", Self::load_graphics()),
        )
            .join()
            .map(|(bodies, palette)| GameState {
                bodies: bodies,
                particle_groups: Vec::new(),
                batch: Batch::new(palette),
                score: 0,
                mouse_clicked: false,
                mouse_start: Point::new(0.0, 0.0),
                mouse_end: Point::new(0.0, 0.0),
            })
    }

    fn interact(&mut self, input: &mut KeyboardAndMouse, _window: &mut Window) {
        let mouse = input.mouse();
        if mouse.is_button_pressed(mouse::Button::Left) && !self.mouse_clicked {
            self.mouse_clicked = true;
            self.mouse_start = mouse.cursor_position();
            self.mouse_end = mouse.cursor_position();
        } else if mouse.is_button_pressed(mouse::Button::Left) && self.mouse_clicked {
            self.mouse_end = mouse.cursor_position();
        } else if !mouse.is_button_pressed(mouse::Button::Left) && self.mouse_clicked {
            self.mouse_clicked = false;
            let difference = -((self.mouse_start - self.mouse_end).normalize());
            let mut rng = rand::thread_rng();
            self.bodies.push(
                Body::new(
                    2,
                    30,
                    self.mouse_start,
                    difference,
                    [1, rng.gen_range(2, 5) as u16],
                    false
                )
            );
        }
    }

    fn update(&mut self, _window: &Window) {
        let other_bodies: Vec<Body> = self.bodies.clone();
        self.bodies.par_iter_mut().for_each(move |body: &mut Body| {
            body.update(&other_bodies);
        });

        let other_bodies: Vec<Body> = self.bodies.clone();
        self.particle_groups.par_iter_mut().for_each(move |group: &mut ParticleGroup| {
            group.update(&other_bodies);
        });
        self.particle_groups.retain(|pg| !pg.group.is_empty());

        self.particle_groups.extend(
            self.bodies.par_iter_mut().filter(|body| body.collision).map(|body| {
                ParticleGroup::new(body.position)
            }).collect::<Vec<ParticleGroup>>()
        );

        self.bodies.retain(|e| !e.collision);
        self.bodies.par_iter_mut().for_each(move |body: &mut Body| {
            body.position = body.next_position
        });

        self.score = self.bodies.iter().map(|body| {
            match body.stabilized {
                true => 1,
                false => 0,
            }
        }).sum();
    }

    fn draw(&mut self, frame: &mut Frame, timer: &Timer) {
        // Clear the current frame
        frame.clear(Color::BLACK);

        let delta_time = timer.next_tick_proximity();

        let body_sprites = self.bodies.par_iter().rev().map(|body| {
            Sprite {
                source: Rectangle {
                    x: (body.sprite[0] - 1) * Self::SPRITE_WIDTH,
                    y: (body.sprite[1] - 1) * Self::SPRITE_HEIGHT,
                    width: Self::SPRITE_WIDTH,
                    height: Self::SPRITE_HEIGHT,
                },
                position: body.position + body.velocity * delta_time,
                scale: (body.radius as f32 / Self::SPRITE_WIDTH as f32, body.radius as f32 / Self::SPRITE_HEIGHT as f32),
            }
        });
        let particles_vec: Vec<Body> = self.particle_groups.clone().par_iter().map(|pg| {
            pg.group.par_iter().map(|p| { p.body })}).flatten().collect();
        let particle_sprites = particles_vec.par_iter().map(|body| {
            Sprite {
                source: Rectangle {
                    x: (body.sprite[0] - 1) * Self::SPRITE_WIDTH,
                    y: (body.sprite[1] - 1) * Self::SPRITE_HEIGHT,
                    width: Self::SPRITE_WIDTH,
                    height: Self::SPRITE_HEIGHT,
                },
                position: body.position + body.velocity * delta_time,
                scale: (body.radius as f32 / Self::SPRITE_WIDTH as f32, body.radius as f32 / Self::SPRITE_HEIGHT as f32),
            }
        });
        self.batch.clear();
        self.batch.par_extend(particle_sprites);
        self.batch.par_extend(body_sprites);
        self.batch.draw(&mut frame.as_target());
    }
}


impl UserInterface for GameState {
    type Message = Message;
    type Renderer = Renderer;

    fn react(&mut self, _msg: Message, _window: &mut Window) {
    }

    fn layout(&mut self, window: &Window) -> Element<Message> {
        Column::new()
            .padding(20)
            .spacing(20)
            .width(window.width() as u32)
            .height(window.height() as u32)
            .justify_content(Justify::End)
            .push(Text::new(
                &format!("Stabilized Planets: {}", self.score))
                .size(50)
                .color(Color::WHITE),
            )
            .into()
    }
}

#[derive(Debug, Clone, Copy)]
pub enum Message {
    ToggleInterpolation(bool),
}

