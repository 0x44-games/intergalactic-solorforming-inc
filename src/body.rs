use snowflake::ProcessUniqueId;

use super::{Point, Vector};

pub const GRAVITY: f64 = 10.0;

#[derive(Clone, Copy, Debug)]
pub struct Body {
    pub id: ProcessUniqueId,
    pub radius: u16,
    pub mass: i32,
    pub position: Point,
    pub next_position: Point,
    pub velocity: Vector,
    pub acceleration: Vector,
    pub static_body: bool,
    pub sprite: [u16; 2],
    pub collision: bool,
    pub stabilized: bool,
}

impl Body {
    pub fn new(mass: i32, radius: u16, position: Point, velocity: Vector, sprite: [u16; 2], static_body: bool) -> Body {
        Body {
            id: ProcessUniqueId::new(),
            radius: radius,
            mass: mass,
            position: position,
            next_position: position,
            velocity: velocity,
            acceleration: Vector::new(0., 0.),
            static_body: static_body,
            sprite: sprite,
            collision: false,
            stabilized: false,
        }
    }

    pub fn center_position(&self) -> Point {
        let mut center = self.position.clone();
        for x in center.iter_mut(){*x += self.radius as f32 / 2.};
        center
    }

    fn check_collision(&self, other: &Body) -> bool {
        if self.id == other.id { return false };
        (self.position.x <= other.position.x + other.radius as f32 &&
         self.position.x + self.radius as f32 >= other.position.x) &&
        (self.position.y <= other.position.y + other.radius as f32 &&
         self.position.y + self.radius as f32 >= other.position.y)
    }

    fn check_stability(&mut self) {
        if !self.collision && !self.static_body { self.stabilized = true }
    }

    fn update_acceleration(&mut self, bodies: &Vec<Body>) {
        self.acceleration = bodies.iter().map(|well| {
            let distance = self.center_position() - well.center_position();
            if distance == (Point::new(0., 0.) - Point::new(0., 0.)) {
                return Point::new(0., 0.) - Point::new(0., 0.)
            }
            -(GRAVITY as f32 * (self.mass * well.mass) as f32 * distance.normalize()
                / distance.norm_squared().max(1000.))
        })
        .sum()
    }

    fn update_position(&mut self) {
        self.velocity += self.acceleration;
        self.next_position += self.velocity;
    }

    pub fn update(&mut self, bodies: &Vec<Body>) {
        if !self.static_body {
            self.update_acceleration(bodies);
            self.update_position();
            self.check_stability();
            self.collision = bodies.iter()
                .map(|body| self.check_collision(body))
                .collect::<Vec<bool>>()
                .iter()
                .any(|x| *x == true);
            // println!("{}, {}",self.acceleration, self.position);
        }
    }
}
