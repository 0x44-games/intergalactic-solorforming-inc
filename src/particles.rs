use rand::prelude::*;
use rayon::prelude::*;
use std::f32::consts::TAU;

use super::{Body, Point, Vector};

#[derive(Clone, Copy, Debug)]
pub struct Particle {
    pub body: Body,
    lifetime: u32,
    age: u32,
}

impl Particle {
    const MAX_LIFE: u32 = 100;

    pub fn new(position: Point, velocity: Vector) -> Particle {
        let body = Body::new(1, 2, position, velocity, [1, 5], false);
        let mut rng = rand::thread_rng();
        let lifetime: u32 = rng.gen_range(Self::MAX_LIFE / 2, Self::MAX_LIFE);
        Particle { body, lifetime, age: 0 }
    }

    pub fn update(&mut self) {
        self.age += 1;
    }
}

#[derive(Clone, Debug)]
pub struct ParticleGroup {
    pub group: Vec<Particle>,
}

impl ParticleGroup {
    const MAX_GROUP_SIZE: i32 = 1000;
    pub fn new(position: Point) -> ParticleGroup {
        let mut group: Vec<Particle> = Vec::new();
        let mut rng = rand::thread_rng();
        for i in fan_out(Self::MAX_GROUP_SIZE) {
            group.push(Particle::new(position, i * rng.gen_range(0.1,2.) as f32));
        }
        ParticleGroup{ group }
    }

    pub fn update(&mut self, bodies: &Vec<Body>) {
        self.group.par_iter_mut().for_each(move |particle: &mut Particle| {
            particle.update();
            particle.body.update(&bodies);
            particle.body.position = particle.body.next_position});
        self.group.retain(|p| p.age < p.lifetime);
    }
}

struct Rot(f32);
impl Rot {
    fn as_vector(self) -> Vector {
        let (y, x) = self.0.sin_cos();
        Vector::new(x, y)
    }
}

fn fan_out(n: i32) -> Vec<Vector> {
    (0..n).map(move |i| Rot((i as f32 / n as f32) * TAU).as_vector()).collect()
}
